<xsl:function name="sfti:validate-date" as="xs:boolean">
        <xsl:param name="date-value" as="xs:string"/>

        <xsl:variable name="parsedDate">
            <xsl:value-of select="sfti:parse-date($date-value)"/>
        </xsl:variable>

        <xsl:variable name="day" select="number(substring($parsedDate, 9, 2))"/>
        <xsl:variable name="month" select="number(substring($parsedDate, 6, 2))"/>
        <xsl:variable name="year" select="number(substring($parsedDate, 1, 4))"/>

        <!-- Vérification de la validité de la date -->
        <xsl:choose>
            <!-- Vérification du format de la date -->
			<!-- 20240123 / 2024-01-23 -->
            <xsl:when
                    test="not(matches($date-value, '^[0-9]{8}$') or matches($date-value, '^\d{4}-\d{2}-\d{2}$'))">
                <xsl:sequence select="false()"/>
            </xsl:when>
            <!-- Vérification du mois de février pour une année bissextile -->
            <xsl:when test="$month = 2">
                <xsl:choose>
                    <!-- Si l'année est bissextile, le mois de février peut aller jusqu'à 29 jours -->
                    <xsl:when
                            test="(($year mod 4 = 0 and $year mod 100 != 0) or ($year mod 400 = 0)) and $day <= 29">
                        <xsl:sequence select="true()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:sequence select="false()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <!-- Vérification des autres mois -->
            <xsl:when
                    test="($day <= 31 and $day > 0) and ($month = 1 or $month = 3 or $month = 5 or $month = 7 or $month = 8 or $month = 10 or $month = 12)">
                <xsl:sequence select="true()"/>
            </xsl:when>
            <xsl:when
                    test="($day <= 30 and $day > 0 ) and ($month = 4 or $month = 6 or $month = 9 or $month = 11)">
                <xsl:sequence select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:sequence select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- parse date -->
    <xsl:function name="sfti:parse-date" as="xs:string">
        <xsl:param name="date" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="$date castable as xs:date">
                <xsl:value-of select="$date"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of
                        select="concat(substring($date, 1, 4), '-', substring($date, 5, 2), '-', substring($date, 7, 2))"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
# XSLT



## Getting started

Validates a date in YYYYMMDD or YYYY-MM-DD format.
check if it is a leap year then the month of February can go up to 29 days otherwise 28 days

For the other months if the month equals 4 or 6 or 9 or 11 then we can go up to 30 otherwise 31 except February

## examples
sfti:validate-date('20240229') return true()

sfti:validate-date('20230229') return false()

sfti:validate-date('2024-02-29') return true()

sfti:validate-date('2023-02-29') return false()

sfti:validate-date('2023-04-31') return false()

sfti:validate-date('20230431') return false()